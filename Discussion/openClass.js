let students = []; 
let sectionedStudents = []; 

function addStudent(name) {
  //call out the array and use the push() to add a new element inside the container.
  students.push(name); 
  //display a return in the console
  console.log(name + ' has been added to the list of students');
}

addStudent('Zyrus');
addStudent('Marie');
addStudent('John'); 
console.log(students); 

function countStudents() {
  //display the number of elements inside the array with the prescribed message. 
  console.log('This Class has a total of '+ students.length +' enrolled students');
} 

//invocation 
countStudents(); 


function printStudents() {
   //sort the elements inside the array in alphanumeric order.
   students = students.sort();
   //console.log(students); 
   //we want to display each element in the console individually.
   students.forEach(function(student) {
   	 console.log(student); 
   })
}

printStudents(); 


function findStudent(keyword) {
  //we have to describe each element inside the array individually.
  let matches = students.filter(function(student) {
  	//we are assessing/evaluating if each          element includes the keyword
  	//keyword = keyword.toLowerCase()
  	//we converted both the keyword and the each element into lowercase characters. 
    return student.toLowerCase().includes(keyword.toLowerCase());   
  }); 
  //console.log(typeof matches) //this a checker
  //create a control structure that will give the proper response according to the result of the filter. 
  if (matches.length === 1) {
     //if there are matches found for the keyword
     console.log(matches[0] + ' is Enrolled'); 
  } else if (matches.length > 1) {
     console.log('Multiple Students Matches this keyword'); 
  } else {
     //no results was found. 
     console.log("No Students matches this keyword")
  }
}

//Z === z
findStudent('r'); 


function addSection(section) {
  //we will place each student inside the students array inside a new array called sectionStudents and we will add a section for each element inside the array.
  sectionStudents = students.map(function(student) {
  	 return student + ' is part of section: ' + section;
  })

  console.log(sectionStudents); 
}
addSection('5'); 

//map() -> will create a new array populated with the elements that passes a codition on a given function/argument.

function removeStudent(name) {
	//search for the index of John in the array
	let result = students.indexOf(name); 
	//console.log(result)
	//create a control structure if a result was found
	if (result >= 0) {
       //we will remove the element using the splice method.
       students.splice(result, 1)
       console.log(students);
  	   console.log(name + ' was removed ');
	} else {
       console.log('No student was Removed'); 
	}
}

removeStudent('Zyrus'); 
// console.log(students.indexOf('Zyrus')); //this will resturn the index number of a certain element.

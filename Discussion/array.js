
//[SECTION] Basic Array Structure
let bootcamp = [
    'Learn HTML',
    'Use CSS', 
    'Understand JS',
    'Maintain MongoDB', 
    'Create components using React'
   ]; 
   
   //[SECTION] Access Elements inside an Array
   //how can we identify the index of an element inside an array?
   console.log(bootcamp); 
   //container/array[indexNumber];
   
   console.log(bootcamp[0]);  //Learn HTML
   console.log(bootcamp[4]); //last element in this example.
   console.log(bootcamp[6]); //if we exceed the number of index we will have an "undefined" return.
   
   //[SECTION] Getting the length of an array structure.
   
   //-> you can have access to the '.length' property similar to what we do with strings.
   console.log(bootcamp.length); //5
   //this is useful for executing that will depend on the number of contents/elements inside the storage. 
   
   // if (bootcamp.length > 5) {
   // 	console.log('This is how long the array is, Do not exceed.'); 
   // }
   
   //[SUB SECTION] HOW TO ACCES THE LAST ELEMENT OF AN ARRAY
   
   //Since the index of an element in an array starts with 0, we have to subtract -1 to the length of the array
   
   console.log(bootcamp.length - 1); //4
   //the element that will be access is the last one inside the collection.
   console.log(bootcamp[bootcamp.length -1]);
   
   console.log('BATCH 145'); 
   
   
   //[SECTION] Array Manipulators
   
   //[SUB-SECTION] Mutators
   
   let bootcampTasks = []; 
   
   //push() -> add an element at the end of the array.
   bootcampTasks.push('Learn Javascript'); 
   bootcampTasks.push('Building a Server using Node');
   bootcampTasks.push('Utilizing Express to build a server');
   
   //pop() -> it remove the last element of the array. and be able to repackage it inside a new variable/container.
   let elementRemovedUsingPop = bootcampTasks.pop();
   console.log(elementRemovedUsingPop);
   
   //unshift() -> adds one or more element at the 'front' of the array. 
   bootcampTasks.unshift('Understand the concept of REST API', 
       'How to use Postman', 
       'Learn how to use MongoDB'
   );
   
   //shift() -> removes the first element at the front of the array. we can also take the removed element and place it inside a new variable.
   let akoNatanggalDahilKayShift = bootcampTasks.shift(); 
   console.log(akoNatanggalDahilKayShift); 
   
   
   //splice() -> we can extract and insert new values to the container.
   // syntax:  arrayName.splice(num[startPosition], [num#ofElementsToRemove], OPTIONAL [elementsToBeAdded]); 
   
   //identify where the extraction will begin. 
   //i want to remove all element inside the current array. 
   bootcampTasks.splice(1, 2); 
   //the additional elements will be added to the front of the array.
   //start of extraction = 0
   //end of extraction   = 4 - 1 = 3
   
   // console.log(bootcampTasks); 
   
   //sort() -> rearrange and organize the elements inside the array. 
   
   let library = ['Pride and Prejudice', 'The Alchemist', 'Diary of a pulubi', 'Beauty and the Beast'];
   let series = [9,8,7,6,5,15,89,27,36, 'Apple', 'Abacus', 'Narra'];  
   series.sort(); 
   //the values per index was also changed. 
   
   //library.sort(); 
   
   //reverse() -> reverse the order of each element inside an array. 
   // series.reverse();
   
   console.log(series); 
   console.log(library); 
   
   //[SUB-SECTION] Accessors
   
   // indexof() -> find/identify the index number of a given element.
   //                0     1      2     3      4     5     6       7
   let countries = ['US', 'PH', 'CAN', 'SG', 'CAN', 'JP', 'HON', 'CAN']; 
   //What if you want to target a specific in order to get its index number?
   //in case of duplicate values it will return the 1st instance of the value.
   
   let indexCount = countries.indexOf('CAN');
   console.log('it is located at index :' + indexCount);   
   
   //lastIndexOf() -> where the last/final instance of the element happened. 
   let lastFound = countries.lastIndexOf('CAN'); //7
   console.log('The element was last found at index: ' + lastFound); 
   
   //[SUB-SECTION] Iterators
   
   //What is you want to retrieve each element inside an array. 
   //display all values in the array inside the console.
   //syntax:  array.forEach( function(){ WHAT TO DO TO EACH ELEMENT })
   //were going to pass a parameter inside the function that will descibe each single element inside the array. 
   bootcamp.forEach( function(task) {
      //display each element individually inside the console.
      console.log(task); 
   })
   
   //map() 
   let words = ['Abacus', 'Abacus', 'Abacus', 'Abacus', 'Apple']
   
       //pass an argument inside the function to identify each element inside the array. 
       // numbers.map( function(num) {
       // 	//this will tell the function what to do for each element.
       // 	//only return the values in the series that will be the result of each number multiplied by itself. 
       // 	console.log(num * num);  
       // })
              
   
   
   //every() -> will check all the elements that will pass a certain condition. 
   let isPassed = words.every( function(word) {
       //we can specify the return.
       return (word === 'Abacus');  
   })
   console.log(isPassed); 
   